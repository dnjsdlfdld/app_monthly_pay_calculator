import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CalForm extends StatefulWidget {
  const CalForm({super.key, required this.title});

  final String title;

  @override
  State<CalForm> createState() => _CalFormState();
}


class _CalFormState extends State<CalForm> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _monthlyPayError = false;
  bool _minusPayError = false;

  num _payNationalPerson = 0;
  num _payHealthInsurance = 0;
  num _payLongTermCare = 0;
  num _payEmploymentInsurance = 0;
  num _payIncomeTax = 0;
  num _payLocalIncomeTax = 0;
  num _totalAmountDeducted = 0;
  num _resultMonthlyPay = 0;

  void _calculatorMonthlyPay(num originMonthlyPay, num minusPay) {
    // 과세금액 계산
    num taxationPay = originMonthlyPay - minusPay; // 월급 - 비과세액 = 과세금액

    // 국민연금 계산
    setState(() {
      _payNationalPerson = (taxationPay * 0.045).ceil(); // 과세금액 * 0.045 = 국민연금
      // 건강보험 계산
      _payHealthInsurance = (taxationPay * 0.03495).ceil(); // 과세금액 * 0.3495 = 건강보험
      // 장기요양
      _payLongTermCare = (_payHealthInsurance * 0.1227).ceil(); // 건강보험 * 0.1227 = 장기요양
      // 고용보험
      _payEmploymentInsurance = (taxationPay * 0.009).ceil(); // 과세금액 * 0.009 = 고용보험

      // 소득세
      num taxPercent;
      if (taxationPay < 30000000) {
        taxPercent = 0.02;
      } else if (taxationPay < 60000000) {
        taxPercent = 0.04;
      } else {
        taxPercent = 0.05;
      }

      _payIncomeTax = (taxationPay * taxPercent).ceil();

      // 지방소득세
      _payLocalIncomeTax = (_payIncomeTax * 0.1).ceil(); // 소득세 * 0.1 = 지방소득세

      // 공제액 합계
      _totalAmountDeducted =
      (_payNationalPerson + _payHealthInsurance + _payLongTermCare +
              _payEmploymentInsurance
              + _payIncomeTax + _payLocalIncomeTax).ceil(); // Total 세금

      // 월 예상 실수령해
      _resultMonthlyPay = (originMonthlyPay - _totalAmountDeducted).ceil();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              onChanged: () {
                _formKey.currentState!.save();
                debugPrint(_formKey.currentState!.value.toString());
              },
              autovalidateMode: AutovalidateMode.disabled,
              initialValue: const {
                'monthlyPay' : '',
                'minusPay' : ''
              },
              skipDisabled: true,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: FormBuilderTextField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      name: 'monthlyPay',
                      decoration: InputDecoration(
                        labelText: '월급액',
                        suffixIcon: _monthlyPayError
                            ? const Icon(Icons.error, color: Colors.red)
                            : const Icon(Icons.check, color: Colors.green),
                      ),
                      onChanged: (val) {
                        setState(() {
                          _monthlyPayError = !(_formKey.currentState?.fields['monthlyPay']
                              ?.validate() ??
                              false);
                        });
                      },
                      // valueTransformer: (text) => num.tryParse(text),
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(errorText: "월급을 입력해주세요"),
                        FormBuilderValidators.numeric(errorText: "숫자만 입력 가능합니다."),
                        FormBuilderValidators.min(100, errorText: "최소값은 100원 입니다."),
                        FormBuilderValidators.max(100000000, errorText: "최대값은 1억 입니다."),
                      ]),
                      // initialValue: '12',
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.next,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: FormBuilderTextField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      name: 'minusPay',
                      decoration: InputDecoration(
                        labelText: '비과세액',
                        suffixIcon: _minusPayError
                            ? const Icon(Icons.error, color: Colors.red)
                            : const Icon(Icons.check, color: Colors.green),
                      ),
                      onChanged: (val) {
                        setState(() {
                          _minusPayError = !(_formKey.currentState?.fields['minusPay']
                              ?.validate() ??
                              false);
                        });
                      },
                      onTap: () {
                        FocusScopeNode currentFocus = FocusScope.of(context);
                        currentFocus.unfocus();
                      },
                      // valueTransformer: (text) => num.tryParse(text),
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(errorText: "비과세액을 입력해주세요"),
                        FormBuilderValidators.numeric(errorText: "숫자만 입력 가능합니다."),
                        FormBuilderValidators.min(100, errorText: "최소값은 100원 입니다."),
                        FormBuilderValidators.max(10000000, errorText: "최대값은 1천만원 입니다."),
                      ]),
                      // initialValue: '12',
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.next,
                    ),
                  ),
                  const SizedBox(height: 30),
                  ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState?.saveAndValidate() ?? false) {
                        debugPrint(_formKey.currentState?.value.toString());
                        _calculatorMonthlyPay(
                          // !는 필드가 꼭 있어야 합니다
                          // num.parse 는 스트링에서 숫자로
                            num.parse(_formKey.currentState!.fields['monthlyPay']!.value),
                            num.parse(_formKey.currentState!.fields['minusPay']!.value)
                        );
                      }
                    },
                    child: const Text(
                      '계산하기',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text('국민연금', style: TextStyle(color: Colors.brown, fontSize: 20)),
                Text(_payNationalPerson.toString() + "원")
              ],
            ),
            const SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text('건강보험', style: TextStyle(color: Colors.brown, fontSize: 20)),
                Text(_payHealthInsurance.toString() + " 원")
              ],
            ),
            const SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text('장기요양', style: TextStyle(color: Colors.brown, fontSize: 20)),
                Text("$_payLongTermCare 원")
              ],
            ),
            const SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text('고용보험', style: TextStyle(color: Colors.brown, fontSize: 20)),
                Text("$_payEmploymentInsurance 원")
              ],
            ),
            const SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text('소득세', style: TextStyle(color: Colors.brown, fontSize: 20)),
                Text("$_payIncomeTax 원")
              ],
            ),
            const SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text('지방소득세', style: TextStyle(color: Colors.brown, fontSize: 20),),
                Text("$_payLocalIncomeTax 원")
              ],
            ),
            const SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text(
                  '공제액 합계',
                  style: TextStyle(
                      color: Colors.brown, fontSize: 20
                  ),
                ),
                Text(_totalAmountDeducted.toString() + " 원")
              ],
            ),
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text(
                  '월 예상 실수령액 :',
                  style: TextStyle(
                      fontSize: 30
                  ),),
                Text(
                  _resultMonthlyPay.toString() + " 원",
                  style: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w500
                  ),
                )
              ],
            ),
          ],
        ),
      )
    );
  }
}
