# 월급 계산기 APP
***
### LANGUAGE
```
* Dart
* Flutter
```
***
### 기능
```
    - flutter_form_builder 패키지 활용
    - form_builder_validators 패키지 활용
    - 4대보험 공제 % 계산식 작성
```
![covid_clinic](./assets/monthly.png)